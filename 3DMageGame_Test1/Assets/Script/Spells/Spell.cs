﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpellType
{
    DAMAGE,
    HEAL
}
public abstract class Spell : MonoBehaviour
{
    public GameObject SpellPrefab;
    public SpellType type;
    public string spellName = default;
    [TextArea(10,15)]
    public string description;
    public bool canHold = false;
    public float spellCooldown;
    public bool onCooldown = false;
    public virtual void ActivateSpell(Transform firePoint)
    {

    }
    public virtual void ActivateSpell(Transform firePoint,bool active)
    {

    }
    public virtual void ActivateSpellHolding(Transform firePoint, bool active)
    {

    }

    
}
