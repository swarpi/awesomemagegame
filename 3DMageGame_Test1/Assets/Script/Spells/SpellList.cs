﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="New SpellList",menuName ="Spell System/SpellList")]
public class SpellList : ScriptableObject
{
    [SerializeField] List<Spell> list = new List<Spell>();
    public List<Spell> Spells => list;

    public void resetCoolDown()
    {
        list.ForEach(x => x.onCooldown = false);
    }

}
