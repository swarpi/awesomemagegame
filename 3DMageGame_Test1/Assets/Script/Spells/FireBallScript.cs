﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallScript : DamageSpell
{


    private void Start()
    {
        
    }
    public override void ActivateSpell(Transform firePoint, bool active)
    {
        
        //Debug.Log(firePoint.rotation);
        Debug.Log("spell rotation is " + this.transform.rotation);
        GameObject spell = Instantiate(this.gameObject, firePoint.position, firePoint.rotation * Quaternion.Euler(0, 0, 90));
        Rigidbody rigidbody = spell.GetComponent<Rigidbody>();
        rigidbody.AddForce(firePoint.up * spellSpeed, ForceMode.Impulse);


        //TODO spell in the right direction
    }
    private new void OnCollisionEnter(Collision collision)
    {
        //base.OnCollisionEnter(collision);

        if (collision.gameObject.tag == "Enemy")
        {
            DamageEnemy(collision.gameObject);
        }
        Destroy(this.gameObject);
    }
    void DamageEnemy(GameObject Enemy)
    {
        Debug.Log("Hit " + Enemy.name);

        Enemy.GetComponent<EnemyStats>().TakeDamage(spellDamage,this.gameObject);
    }
}
