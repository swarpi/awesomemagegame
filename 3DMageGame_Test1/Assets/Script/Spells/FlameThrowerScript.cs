﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameThrowerScript : DamageSpell
{
    private new void Awake()
    {
        canHold = true;
    }
    public override void ActivateSpell(Transform firePoint, bool active)
    {
        if (active == false)
        {
            Destroy(spell);
        }
        if (!spell)
        {
            spell = Instantiate(this.gameObject, firePoint.position, firePoint.rotation * Quaternion.Euler(0, 0, 90));
            spell.transform.parent = firePoint.transform;
        }
    }
}
