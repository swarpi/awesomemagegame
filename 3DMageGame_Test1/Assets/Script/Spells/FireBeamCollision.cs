﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBeamCollision : MonoBehaviour
{
    public GameObject explosion;
    public Vector3 destroyPosition;
    private void Start()
    {

    }

    void InitializeIfNeeded()
    {

    }


    private void OnParticleCollision(GameObject other)
    {
        Destroy(this.gameObject);
        Instantiate(explosion, this.transform.position, other.transform.rotation);
        Debug.Log(other);
        
    }
}
