﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageSpell : Spell
{
    public float spellSpeed = 5f;
    public int spellDamage = 5;
    public GameObject player;
    protected GameObject spell;
    

    public void Awake()
    {
        type = SpellType.DAMAGE;
        
    }

    public override void ActivateSpell(Transform firePoint, bool active)
    {
        //Debug.Log(firePoint.rotation);
        Debug.Log("spell rotation is " + this.transform.rotation);
        GameObject Spell = Instantiate(this.gameObject, firePoint.position, firePoint.rotation * Quaternion.Euler(0, 0, 90));
        if (Spell.GetComponent<Rigidbody2D>())
        {
            Rigidbody2D rigidbody2D = Spell.GetComponent<Rigidbody2D>();
            rigidbody2D.AddForce(firePoint.up * spellSpeed, ForceMode2D.Impulse);
        }
        
        //TODO spell in the right direction
    }

    public override void ActivateSpellHolding(Transform firePoint,bool active)
    {
        if(active == false)
        {
            Destroy(spell);
        }
        if (!spell)
        {
            spell = Instantiate(this.gameObject, firePoint.position, firePoint.rotation * Quaternion.Euler(0, 0, 90));
            spell.transform.parent = firePoint.transform;
        }
    }
    protected void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            DamageEnemy(collision.gameObject);
        }
        Destroy(this.gameObject);
    }

    void DamageEnemy(GameObject Enemy)
    {
        Debug.Log("Hit " + Enemy.name);

        Enemy.GetComponent<CharacterStats>().TakeDamage(spellDamage,this.gameObject);
    }
}
