﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CameraFollow : MonoBehaviour
{
    public Transform target;

    public float smoothing = 0.15f;

    public Vector3 velocity = Vector3.one;

    public LayerMask layer;

    public Vector3 offset;
    //public Camera cam;
    public float maximumLength;
    public Vector3 minPosition;
    public Vector3 maxPosition;

    private Ray rayMouse;
    public HashSet<RaycastHit> set = new HashSet<RaycastHit>();
    private RaycastHit[] hits = null;

    void LateUpdate()
    {
        Vector3 desiredPos = target.position + offset;
        Debug.Log(desiredPos);
        desiredPos.x = Mathf.Clamp(desiredPos.x, minPosition.x, maxPosition.x);
        desiredPos.z = Mathf.Clamp(desiredPos.z, minPosition.z, maxPosition.z);
        Vector3 smoothedPosition = Vector3.SmoothDamp(transform.position, desiredPos,ref velocity, smoothing * Time.deltaTime);
       
        transform.position = smoothedPosition;
        
        
        //transform.LookAt(target, target.up);
    }

    private void Update()
    {
        // as long as object is in list make it transparant
        // when object is no longer in list make it normal again
        // 
        
        Debug.DrawRay(transform.position, (target.transform.position - transform.position), Color.magenta);

        //RaycastHit[] hits = Physics.RaycastAll(transform.position, (target.transform.position - transform.position), Vector3.Distance(transform.position, target.transform.position), layer);
        RaycastHit[] hits = Physics.SphereCastAll(transform.position,0.1f, (target.transform.position - transform.position), Vector3.Distance(transform.position, target.transform.position)- 0.1f, layer);
        List<RaycastHit> hitList = hits.ToList<RaycastHit>();
        
        if (hitList != null)
        {
            foreach (RaycastHit hit in hitList)
            {
                set.Add(hit);
                Renderer r = hit.collider.GetComponent<Renderer>();
                if (r)
                {
                    SetMaterialTransparent(r.materials);
                }

            }
            foreach(RaycastHit noLonger in set){;
                if (!hitList.Contains(noLonger))
                {
                    StartCoroutine(SetMaterialOpague(noLonger.collider.GetComponentInChildren<Renderer>().materials));
                    //set.Remove(noLonger);
                }
            }
            set.RemoveWhere(x => !hitList.Contains(x));

        }
    }

    void SetMaterialTransparent(Material[] materials)
    {
        foreach(Material m in materials)
        {
            m.SetFloat("_Mode", 3);
            var tempcolor = m.color;
            tempcolor.a = 0.15f;
            m.color = tempcolor;
            m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            m.SetInt("_ZWrite", 0);
            m.DisableKeyword("_ALPHATEST_ON");
            m.EnableKeyword("_ALPHABLEND_ON");
            m.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            m.renderQueue = 3000;

        }
    }
    IEnumerator SetMaterialOpague(Material[] materials)
    {
        yield return new WaitForFixedUpdate();
        foreach(Material m in materials) { 
            m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
            m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
            m.SetInt("_ZWrite", 1);
            m.DisableKeyword("_ALPHATEST_ON");
            m.DisableKeyword("_ALPHABLEND_ON");
            m.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            m.renderQueue = -1;
        }
    }
} 
