﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransferRoom : MonoBehaviour
{

    public Vector3 cameraChange;
    public Vector3 playerChange;
    public CameraFollow cam;

    private void Start()
    {
        cam = Camera.main.GetComponent<CameraFollow>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            cam.minPosition += cameraChange;
            cam.maxPosition += cameraChange;
            other.transform.position += playerChange;
        }
    }
}

