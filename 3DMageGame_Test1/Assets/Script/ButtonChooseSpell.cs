﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonChooseSpell : MonoBehaviour
{
    [SerializeField] Button[] actionButtons;
    public bool[] spellOnCooldown;
    private KeyCode action1, action2, action3;
    public Image frame;
    public SpellList spellList;
    private void Start()
    {
        spellOnCooldown = new bool[3];
        action1 = KeyCode.Alpha1;
        action2 = KeyCode.Alpha2;
        action3 = KeyCode.Alpha3;
    }
    // frame change pos
    private void LateUpdate()
    {
        if (Input.GetKeyDown(action1))
        {
            ActionButtonOnClick(0);
        }
        if (Input.GetKeyDown(action2))
        {
            ActionButtonOnClick(1);
        }
        if (Input.GetKeyDown(action3))
        {
            ActionButtonOnClick(2);
        }

        if (spellList.Spells[0].onCooldown)
        {
            SetOnCooldown(0, spellList.Spells[0].spellCooldown, true);
            Debug.Log("spell on cooldown");
            
        }
        else 
        {
            actionButtons[0].gameObject.transform.GetChild(0).GetComponent<Image>().fillAmount = 0;
        }

        if (spellList.Spells[1].onCooldown)
        {
            SetOnCooldown(1, spellList.Spells[1].spellCooldown, true);
        }
        else
        {
            actionButtons[1].gameObject.transform.GetChild(0).GetComponent<Image>().fillAmount = 0;
        }
        if (spellList.Spells[2].onCooldown)
        {
            SetOnCooldown(2, spellList.Spells[2].spellCooldown, true);
        }
        else
        {
            actionButtons[2].gameObject.transform.GetChild(0).GetComponent<Image>().fillAmount = 0;
        }


    }
    public void Click(int index)
    {
        
        GameObject.FindGameObjectWithTag("Player").SendMessage("chooseSpell",index);
    }
    void ActionButtonOnClick(int btnIndex)
    {
        frame.rectTransform.localPosition = actionButtons[btnIndex].transform.localPosition;
        //frame.rectTransform.forward = actionButtons[btnIndex].transform.position;
        actionButtons[btnIndex].onClick.Invoke(); // acts like if we click the button
    }

    public void SetOnCooldown(int btnIndex, float cooldownTime, bool OnCooldown)
    {
        if (actionButtons[btnIndex].gameObject.transform.GetChild(0).GetComponent<Image>().fillAmount <= 0)
        {
            actionButtons[btnIndex].gameObject.transform.GetChild(0).GetComponent<Image>().fillAmount = 1;
        }
        actionButtons[btnIndex].gameObject.transform.GetChild(0).GetComponent<Image>().fillAmount -= 1/cooldownTime * Time.deltaTime;

       

    }

}
