﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestGenerator : MonoBehaviour
{
    public int forestSize = 25;
    public int elememtSpacing = 3; // every 3 unit in world we put an element down

    public Element[] elements;
    System.Random rnd = new System.Random();
    public Vector3[,] terrainDrawn;

    private void Start()
    {
        //terrain = GetComponent<TerrainGrid>();
        //Debug.Log(terrain);
        //terrainDrawn = terrain.DrawGrid();
        //Debug.Log(terrainDrawn[0,0]);
        //GenerateForest(forestSize);
        
    }

    public void GenerateForest(int size)
    {
        for (int x = 0; x < size; x += elememtSpacing)
        {
            for (int z = 0; z < size; z += elememtSpacing)
            {
                Element element = elements[rnd.Next(0, 2)];
                if (element.CanPlace())
                {
                    Debug.Log(terrainDrawn[x, z]);
                    Vector3 position = terrainDrawn[x, z]; // atm all is flat thats why y = 0f
                    Vector3 offSet = new Vector3(Random.Range(-0.75f, 0.75f), 0f, Random.Range(-0.75f, 0.75f)); // offset between objects
                    Vector3 rotation = new Vector3(Random.Range(0, 5f), Random.Range(0, 360f), Random.Range(0, 5f));
                    Vector3 scale = Vector3.one * Random.Range(0.75f, 1.25f);
                    GameObject newElement = Instantiate(element.GetRandom());
                    newElement.transform.SetParent(transform);
                    newElement.transform.position = position + offSet;
                    newElement.transform.eulerAngles = rotation;
                    newElement.transform.localScale = scale;
                }
            }

        }
    }

    public void GenerateForestWithTerrain(int size, Vector3[,] terrainDrawn, TerrainManager TM)
    {
        for (int x = 0; x < size; x += elememtSpacing)
        {
            for (int z = 0; z < size; z += elememtSpacing)
            {
                
                if (TM.GetTerrainAtPosition(terrainDrawn[x, z]) != 0)
                {
                    Element element = elements[rnd.Next(0, elements.Length)];
                    if (element.CanPlace())
                    {
                        Vector3 position = terrainDrawn[x, z]; // atm all is flat thats why y = 0f
                        Vector3 offSet = new Vector3(Random.Range(-0.75f, 0.75f), 0f, Random.Range(-0.75f, 0.75f)); // offset between objects
                        Vector3 rotation = new Vector3(Random.Range(0, 5f), Random.Range(0, 360f), Random.Range(0, 5f));
                        Vector3 scale = Vector3.one * Random.Range(0.75f, 1.25f);
                        GameObject newElement = Instantiate(element.GetRandom());
                        newElement.transform.SetParent(transform);
                        newElement.transform.position = position + offSet;
                        newElement.transform.eulerAngles = rotation;
                        newElement.transform.localScale = scale;
                    }
                }
            }

        }
    }
}



[System.Serializable]
public class Element
{
    public string name;
    [Range(1,10)]
    public int density;

    public GameObject[] prefabs;

    public bool CanPlace()
    {
        if(Random.Range(0,10) < density)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public GameObject GetRandom()
    {
        return prefabs[Random.Range(0, prefabs.Length)];
    }
}