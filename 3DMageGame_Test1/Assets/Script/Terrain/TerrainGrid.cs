﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGrid : MonoBehaviour
{
    public Vector3 GridSize;
    public Vector3[,] grid;
    public Transform squarePrefab;
    public TerrainManager TM;
    public ForestGenerator FG;
    // Start is called before the first frame update
    void Start()
    {
        GridSize = GetComponent<Terrain>().terrainData.size;
        TM = this.GetComponent<TerrainManager>();
        FG = this.GetComponent<ForestGenerator>();
        grid = new Vector3[(int)GridSize.x, (int)GridSize.z];
        //DrawGrid();
        DrawForest();
    }
    // zeichnet ein Grid auf dem Terrain und liefert ein 2D array mit den positionen der tiles zurueck
    public Vector3[,] DrawGrid()
    {
        for(int x = 0; x< GridSize.x; x++)
        {
            for (int z = 0; z < GridSize.z; z++)
            {
                Vector3 tilePosition = new Vector3(transform.position.x + x, 0.2f, transform.position.z + z);
                //Transform newTile = Instantiate(squarePrefab, tilePosition, Quaternion.Euler(Vector3.right * 90)) as Transform;
                //newTile.transform.SetParent(transform);
                grid[x, z] = tilePosition;
                //Debug.Log(grid[x, z]);
                //newTile.name = "Tile_" + "_x" + x + "_z" + z + "With terrain_" + TM.GetTerrainAtPosition(grid[x, z]);
            }
        }
        return grid;
    }

    public void DrawForest()
    {
        FG.GenerateForestWithTerrain(25, DrawGrid(),TM);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
