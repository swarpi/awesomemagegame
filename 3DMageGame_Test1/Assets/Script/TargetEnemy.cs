﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetEnemy : MonoBehaviour
{

    public Transform target;

    private void Start()
    {
        target = GameObject.Find("GhostEnemy").transform;
    }

    private void Update()
    {
        InLineOfSight();
    }
    private bool InLineOfSight()
    {
        if (target) { 
        Vector4 targetDirec = (target.transform.position - transform.position).normalized;

        Debug.DrawRay(transform.position, targetDirec, Color.red);
        }

        return false;
    }
}
