﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineLookat : MonoBehaviour
{
    public Camera cam;
    public float dist;

    private OutlineController prevController;
    private OutlineController currController;

    void Update()
    {
        HandleLookAtRay();
    }

    void HandleLookAtRay()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if(Physics.Raycast(ray,out hit, dist))
        {
            if (hit.collider.CompareTag("interObject"))
            {
                currController = hit.collider.GetComponent<OutlineController>();

                if(prevController != currController)
                {
                    Debug.Log("going in");
                    HideOutline();
                    ShowOutline();
                }
                prevController = currController;
            }
            else
            {
                HideOutline();
            }
        }
        else
        {
            HideOutline();
        }
    }

    private void ShowOutline()
    {
        if(currController != null)
        {
            Debug.Log("showing outline" + currController.gameObject);
            currController.ShowOutline();
            currController.ShowText();
        }
    }

    private void HideOutline()
    {
        if(prevController!= null)
        {
            Debug.Log("hiding outline");
            prevController.HideOutLine();
            currController.HideText();
            prevController = null;
        }
    }
}
