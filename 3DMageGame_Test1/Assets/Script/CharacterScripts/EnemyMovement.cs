﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public Transform player;
    public float wanderSpeed = 1f;
    public float runSpeed = 5f;
    public float turnSpeed = 5f;
    private Animator animator;

    int triggerLayer = 0;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        triggerLayer = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void FixedUpdate()
    {
        if (triggerLayer == 0)
        {
            MoveCharacterWander(player.position);
        }
        if(triggerLayer > 0)
        {
            MoveCharacterRunning(player.position);
        }

    }
    void MoveCharacterWander(Vector3 direction)
    {
        // transform.LookAt(player.transform);
        Quaternion targetRotation = Quaternion.LookRotation(player.transform.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, turnSpeed * Time.deltaTime);
        transform.position += transform.forward * wanderSpeed * Time.deltaTime;
    }

    void MoveCharacterRunning(Vector3 direction)
    {
        // transform.LookAt(player.transform);
        Quaternion targetRotation = Quaternion.LookRotation(player.transform.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, turnSpeed * Time.deltaTime);
        transform.position += transform.forward * runSpeed * Time.deltaTime;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            triggerLayer++;
            if (triggerLayer == 1)
            {
                animator.SetBool("Aware", true);
                // animation that player was spotted
                
            }
            if (triggerLayer == 2)
            {
                //animator.SetBool("Aware", false);
                animator.SetBool("InAttackRange", true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            triggerLayer--;
            if(triggerLayer == 1)
            {
                animator.SetBool("InAttackRange", false);
            }
            if(triggerLayer == 0)
            {
                animator.SetBool("Aware", false);
            }
        }
        
    }

}
