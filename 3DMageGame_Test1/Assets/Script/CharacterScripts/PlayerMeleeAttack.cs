﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMeleeAttack : MonoBehaviour
{
    private float nextMeeleAttackTime = 0f;
    public float meeleCoolDownTime; // must be greater than 1
    bool meeleOnCooldown = false;
    public Animator animatior;

    private void Start()
    {
        animatior.SetBool("attacking", false);
    }

    void Update()
    {
        if (!meeleOnCooldown) // then you can attack
        {
            if (Input.GetKey("mouse 0"))
            {
                //animatior.Play("SwordAnimation1");
                StartCoroutine(MeeleAttack());
                meeleOnCooldown = true;
                nextMeeleAttackTime = meeleCoolDownTime;
            }
        }
        if(nextMeeleAttackTime > 1)
        {
            nextMeeleAttackTime -= Time.deltaTime;
        }
        else
        {
            meeleOnCooldown = false;
        }
    }

    IEnumerator MeeleAttack()
    {
        animatior.SetBool("attacking", true);

        yield return new WaitForSeconds(0.5f);

        animatior.SetBool("attacking", false);
    }
}
