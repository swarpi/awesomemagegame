﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlayerToMouse : MonoBehaviour
{
    public Camera cam;
    public float maximumLength;

    private Ray rayMouse;
    private Vector3 pos;
    private Vector3 direction;
    private Quaternion rotation;
    Vector3 fixPos;
    private void Update()
    {
        if(Input.GetKeyDown("mouse 1"))
        {
            RotateToMouseDirection();
        }
    }
    void RotateToMouseDirection()
    {
            RaycastHit hit;
            var mousePos = Input.mousePosition;
            rayMouse = cam.ScreenPointToRay(mousePos);
            float midPoint = (transform.position - cam.transform.position).magnitude * 0.5f;
            // Ray that points towards the mouseposition on the map
            if (Physics.Raycast(rayMouse.origin, rayMouse.direction, out hit, maximumLength))
            {
                transform.LookAt(hit.point); // look at the ray
                transform.rotation = Quaternion.Euler(0, transform.localEulerAngles.y, 0); // fix rotation of x and z
            }

        }
}
