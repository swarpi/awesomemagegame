﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    private float nextMeeleAttackTime = 0f;
    public float meeleCoolDownTime; // must be greater than 1
    bool meeleOnCooldown = false;
    public Animator animatior;

    public SpellList spellList;
    public Transform firePoint;
    PlayerController controller;
    public int spellIndex;
    bool spellActive = true;
    public Camera cam;
    public float maximumLength;
    float channelTime = 2f;
    public LayerMask layerMaskRotateToMouse;

    private List<Spell> playerSpells;

    public ButtonChooseSpell buttonChooseSpell;

    private Ray rayMouse;
    private Vector3 pos;
    private Vector3 direction;
    private Quaternion rotation;
    Vector3 fixPos;

    void Start()
    {
        controller = GetComponent<PlayerController>();
        spellList.resetCoolDown();
        playerSpells = spellList.Spells;
        animatior.SetBool("attacking", false);
    }

    // Update is called once per frame
    void Update()
    {
        //left click
        if (!meeleOnCooldown) // then you can attack
        {
            if (Input.GetKey("mouse 0"))
            {
                //animatior.Play("SwordAnimation1");

                StartCoroutine(MeeleAttack());
                meeleOnCooldown = true;
                nextMeeleAttackTime = meeleCoolDownTime;
            }
        }
        if (nextMeeleAttackTime > 1)
        {
            nextMeeleAttackTime -= Time.deltaTime;
        }
        else
        {
            meeleOnCooldown = false;
        }

        // right click
        Spell spell = playerSpells[spellIndex];
        if (spell.canHold)
        {
            if (Input.GetKey("mouse 1") && spellActive && !spell.onCooldown)
            {
                if (channelTime < 1)
                {
                    controller.flipable = !controller.flipable;
                    StartCoroutine(UseSpellHold(spell, true));
                    spellActive = !spellActive;
                }
                else
                {
                    channelTime -= Time.deltaTime;
                }
                
            }
            if (Input.GetKeyUp("mouse 1") && !spell.onCooldown) // problem if i double fastclick 
            {
                if (channelTime < 1) 
                { 
                Debug.Log("mouse up");
                StartCoroutine(UseSpellHold(spell, false));
                spellActive = !spellActive;
                }
                channelTime = 2f;
            }
        }
        else
        { 
            if (Input.GetKeyDown("mouse 1") && !spell.onCooldown)
            {
           
                controller.flipable = !controller.flipable;
                StartCoroutine(UseSpell(spell));
 
            }
        }

    }
    // Method for Buttons to choose right spell in spellBar
    public void chooseSpell(int index)
    {
        spellIndex = index;
    }

    IEnumerator UseSpell(Spell spell)
    {
        spell.onCooldown = true;
        
        RotateToMouseDirection();
        // Debug.Log(spell.GetType());
        yield return new WaitForSeconds(0.1f); // short delay so player turn to the mouse and activates spell there
        controller.flipable = !controller.flipable;
        spell.ActivateSpell(firePoint, spellActive);
        yield return new WaitForSeconds(spell.spellCooldown - 0.1f);
        spell.onCooldown = false;
        
    }

    IEnumerator UseSpellHold(Spell spell, bool spellActive)
    {
        
        RotateToMouseDirection();

        // Debug.Log(spell.GetType());
        yield return new WaitForSeconds(0.1f);
        spell.ActivateSpell(firePoint, spellActive);
        if (!spellActive) // only important for flamethrower or spells you can hold. Fix the position while holding the spell
        {
            Debug.Log("cooldown toogling");
            spell.onCooldown = true;
            controller.flipable = !controller.flipable;
            yield return new WaitForSeconds(spell.spellCooldown -0.1f);
            spell.onCooldown = false;

        }
    }

    void RotateToMouseDirection()
    {
        RaycastHit hit;
        var mousePos = Input.mousePosition;
        rayMouse = cam.ScreenPointToRay(mousePos);
        float midPoint = (transform.position - cam.transform.position).magnitude * 0.5f;
        // Ray that points towards the mouseposition on the map
        if (Physics.Raycast(rayMouse.origin, rayMouse.direction, out hit, maximumLength, layerMaskRotateToMouse))
        {
            transform.LookAt(hit.point); // look at the ray
            transform.rotation = Quaternion.Euler(0, transform.localEulerAngles.y, 0); // fix rotation of x and z
        }
    }

    IEnumerator MeeleAttack()
    {
        RotateToMouseDirection();
        animatior.SetBool("attacking", true);

        yield return new WaitForSeconds(0.5f);

        animatior.SetBool("attacking", false);
    }
}
