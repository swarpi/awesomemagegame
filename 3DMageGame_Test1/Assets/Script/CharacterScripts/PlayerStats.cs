﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : CharacterStats
{

    public float knockBackForce;

    public override void Die()
    {
        Debug.Log("Player Died");
    }

    public override void Knockback(Vector3 direction)
    {
        GetComponent<Rigidbody>().AddForce(-direction.normalized * knockBackForce * 10,ForceMode.Impulse);
        StartCoroutine(GetComponent<PlayerController>().ToggleMoveable(1f));
        Debug.Log("knocking player");
    }
}
