﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    [SerializeField] int maxHP = 20;
    public int currentHP;
    [SerializeField] int maxMana;
    int currentMana;
    // Start is called before the first frame update
    void Start()
    {
        currentHP = maxHP;
        currentMana = maxMana;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TakeDamage(int damage, GameObject damageSource)
    {
        Knockback(damageSource.transform.position - transform.position);
        currentHP -= damage;
        if (currentHP <= 0)
        {
            Die();
        }
    }

    public virtual void Die()
    {

    }

    public virtual void Knockback(Vector3 direction)
    {

    }
}
