﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    public GameObject focusObject = null;
    

    void Start()
    {
        //player = GetComponent<PlayerScript>();
    }

    private void Update()
    {
        if (Input.GetKeyDown("e") && focusObject)
        {
            //focusObject.SendMessage("DoInteraction");
            // later add item to inventory
        }
    }



    void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "interObject")
        {
            Debug.Log("we collided with " + collision.gameObject.name);
            focusObject = collision.gameObject;
        }
    }
    private void OnTriggerExit(Collider collision)
    {
        if (collision.CompareTag("interObject"))
        {
            if(collision.gameObject == focusObject)
            {
                focusObject = null;
            }

        }
    }


}
