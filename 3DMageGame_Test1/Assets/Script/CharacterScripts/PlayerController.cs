﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float moveSpeed = 4f;
    Vector3 forward, right;
    public bool flipable = true;
    bool moveable = true;
    Animator animator;

    public Camera cam;
    NavMeshAgent agent;

    Vector3 PreviousFramePosition = Vector3.zero; // Or whatever your initial position is
    float Speed = 0f;


    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        forward = Camera.main.transform.forward.normalized;
        forward.y = 0;
        right = Quaternion.Euler(new Vector3(0, 90, 0)) * forward;
    }

    void Update()
    {
        float movementPerFrame = Vector3.Distance(PreviousFramePosition, transform.position);
        Speed = movementPerFrame / Time.deltaTime;
        PreviousFramePosition = transform.position;
        //Debug.Log(Speed);
        animator.SetFloat("movSpeed", Speed);
        if (Input.GetKeyDown("mouse 0"))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.CompareTag("interObject"))
                {
                    agent.isStopped = false;
                    agent.SetDestination(hit.point);
                    Debug.Log("hit interobejct");
                    // add item to inventory
                }
            }
        }
        if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
        { // weird movement if we walk diagonally cube automatically sets rotation back

            //agent.isStopped = true;
            MoveCharacter();
            
        }

    }




    void MoveCharacter()
    {
        if (moveable) {
            float hor = Input.GetAxisRaw("Horizontal");
            float ver = Input.GetAxisRaw("Vertical");
            Vector3 direction = new Vector3(hor, 0f, ver);
            Vector3 horMovement = right * moveSpeed * Time.deltaTime * hor;
            Vector3 verMovement = forward * moveSpeed * Time.deltaTime * ver;
            transform.position += horMovement;
            transform.position += verMovement;
            Vector3 heading = Vector3.Normalize(horMovement + verMovement);
            if (flipable) {
                transform.forward = heading;
            }
        }
    }

    public IEnumerator ToggleMoveable(float time)
    {
        moveable = false;
        yield return new WaitForSeconds(time);
        moveable = true;

    }
}
