﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineController : MonoBehaviour
{
    private MeshRenderer shadRenderer;

    public float maxOutlineWidth;
    public Color OutlineColor;

    public GameObject text;


    void Start()
    {
        shadRenderer = GetComponent<MeshRenderer>();
    }
    private void Update()
    {

    }

    public void ShowOutline()
    {
        //shadRenderer.material.SetFloat("_Outline", 0.1f);
        shadRenderer.material.SetFloat("_Outline", 0.1f);
        shadRenderer.material.SetColor("_OutlineColor", OutlineColor);
    }
    public void ShowText()
    {
        text.SetActive(true);
    }
    public void HideText()
    {
        text.SetActive(false);
    }

    public void HideOutLine()
    {

        shadRenderer.material.SetFloat("_Outline", 0f);

    }
}
