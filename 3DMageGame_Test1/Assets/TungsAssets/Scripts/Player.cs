﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour
{
    //public MouseItem mouseItem = new MouseItem();

    public InventoryObject inventory;
    public InventoryObject equipment;
    GroundItem groundItem;
    //public GameObject inventoryCanvas;
    //public GameObject characterCanvas;
    //public bool inventoryActive = false;
    //public bool characterActive = false;


    //void Start()
    //{
    //    inventoryCanvas.SetActive(false);
    //   // characterCanvas.SetActive(false);
    //}
    //void ToggleCanvas()
    //{


    //    if (inventoryActive == false)
    //    {
    //        inventoryActive = true;
    //        inventoryCanvas.SetActive(inventoryActive);
    //    }
    //    else
    //    {
    //        inventoryActive = false;
    //        inventoryCanvas.SetActive(inventoryActive);
    //    }


    //}
    //void ToggleCharacter()
    //{


    //    if (characterActive == false)
    //    {
    //        characterActive = true;
    //        characterCanvas.SetActive(characterActive);
    //    }
    //    else
    //    {
    //        characterActive = false;
    //        characterCanvas.SetActive(characterActive);
    //    }


    //}
    public void OnTriggerEnter(Collider other)
    {
        groundItem = other.GetComponent<GroundItem>();
    }

    private void Update()
    {
        
        //Debug.Log("ground item" + groundItem);
        if (Input.GetKeyDown("e"))
        {
            if (groundItem)
            {
                Item _item = new Item(groundItem.item);
                if (inventory.AddItem(_item, 1))
                {
                    Destroy(groundItem.gameObject);
                }
            }
        }
        //if (Input.GetKeyDown("i"))
        //{
        //    ToggleCanvas();
        //}

        //if (Input.GetKeyDown("c"))
        //{
        //    ToggleCharacter();
        //}



        if (Input.GetKeyDown(KeyCode.K))
        {
            inventory.Save(); // refactor into save methods
            equipment.Save();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            inventory.Load();
            equipment.Load();
        }
    }
    private void OnApplicationQuit()
    {
        inventory.Container.Clear();
        equipment.Container.Clear();
    }



}
