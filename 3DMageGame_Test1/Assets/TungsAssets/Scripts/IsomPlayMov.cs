﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsomPlayMov : MonoBehaviour
{
    //OBJECTS VARIABLES
    public Transform CameraTarget;
    float heading = 0;
    public Transform cam;
    CharacterController mover;


    //CAMERA VARIABLES
    Vector3 camF;
    Vector3 camR;

    //INPUT VARIABLES
    Vector2 input;
   

    //PHYSICS VARIABLES
    Vector3 intent;
    Vector3 velocityXZ;
    Vector3 velocity;
    float speed = 5;
    float accel = 11;
    float turnSpeed = 5;
    float turnSpeedLow = 7;
    float turnSpeedHigh = 20;


    //GRAVITY
    float grav = 10;
    public bool grounded = false;

    // Start is called before the first frame update
    void Start()
    {
        mover = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        DoInput();
        CalculateCamera();
        CalculateGround();
        DoMove();
        DoGravity();
        DoJump();

        mover.Move(velocity * Time.deltaTime);
    }

    void DoInput()
    {
        heading += Input.GetAxis("CameraButtonMovement") * Time.deltaTime * 90;
        CameraTarget.rotation = Quaternion.Euler(0, heading, 0);

        //moving
        input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        input = Vector2.ClampMagnitude(input, 1);

    }
    void CalculateCamera()
    {
        camF = cam.forward;
        camR = cam.right;
        camF.y = 0;
        camR.y = 0;
        camF = camF.normalized;
        camR = camR.normalized;

    }
    void CalculateGround()
    {
        //activate fallgravity while in air and turns off fallgravity when hitting the ground
        RaycastHit hit;
        if (Physics.Raycast(transform.position + Vector3.up * 0.1f,-Vector3.up,out hit, 0.2f))
        {
            grounded = true;
        }
        else
        {
            grounded = false;
        }
    }
    void DoMove()
    {
        //move with the camera view
        intent = camF * input.y + camR * input.x;

        //change turning speed(turn faster when standing still turn slower while moving)
        float tS = velocity.magnitude/5;
        turnSpeed = Mathf.Lerp(turnSpeedHigh, turnSpeedLow, tS);
        // player is facing the direction its running
        if (input.magnitude > 0)
        {
            Quaternion playRot = Quaternion.LookRotation(intent);
            transform.rotation = Quaternion.Lerp(transform.rotation, playRot, turnSpeed * Time.deltaTime);
        }
        
        //moving
        velocityXZ = velocity;
        velocityXZ.y = 0;
        velocityXZ = Vector3.Lerp(velocityXZ, transform.forward*input.magnitude*speed, accel*Time.deltaTime);
        velocity = new Vector3(velocityXZ.x, velocity.y, velocityXZ.z);
       
    }

    void DoGravity()
    {
        if (grounded)
        {
            velocity.y = -0.5f;
        }
        else
        {
            velocity.y -= grav * Time.deltaTime;
            velocity.y = Mathf.Clamp(velocity.y, -10, 10);
        }
    }

    void DoJump()
    {
        if (grounded)
        {
            if (Input.GetButtonDown("Jump"))
            {
                velocity.y = 10; 
            }
        }
    }





}
