﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsomeetricPlayerMovement : MonoBehaviour
{
    [SerializeField]
    float moveSpeed = 5;
   
    Vector3 forward, right;
    
   
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        forward = Camera.main.transform.forward;
        forward.y = 0;
        forward = Vector3.Normalize(forward);
        right = Quaternion.Euler(new Vector3(0, 90, 0)) * forward;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey)
        {
            Move();
        }


        

    }

    void Move()
    {
    
         
        Vector3 direction = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        Vector3 rightMovement = right * moveSpeed * Time.deltaTime * Input.GetAxis("HorizontalKey");
        Vector3 upMovement = forward * moveSpeed * Time.deltaTime * Input.GetAxis("VerticalKey");
       

        // Aendert die BlickRichtung des Koerpers in die Richtung in der man sich bewegt
        Vector3 heading = Vector3.Normalize(rightMovement + upMovement);
        transform.forward = heading;


        transform.position += rightMovement; 
        transform.position += upMovement;

       
        

        
    }
}
